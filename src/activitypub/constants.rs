// https://www.w3.org/TR/activitypub/#server-to-server-interactions
pub const AP_MEDIA_TYPE: &str = r#"application/ld+json; profile="https://www.w3.org/ns/activitystreams""#;
pub const AS_MEDIA_TYPE: &str = "application/activity+json";

pub const AP_CONTEXT: &str = "https://www.w3.org/ns/activitystreams";
pub const AP_PUBLIC: &str = "https://www.w3.org/ns/activitystreams#Public";

pub const ACTOR_KEY_SUFFIX: &str = "#main-key";
